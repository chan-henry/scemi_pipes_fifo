module top;
	parameter clockLow  = 1;
	parameter clockHigh = 1;
	parameter NUM_OF_EDGES_RESET = 3;

	bit clk;
	bit rst;

	bit re;
	bit we;
	bit [ 31 : 0 ] data_in;
	bit [ 31 : 0 ] data_out;
	bit full;
	bit empty;
	
	FifoTransactor transactor (
		.clk		( clk		),
		.rst		( rst		),
		.re		( re		),
		.we		( we		),
		.data_in	( data_in	),
		.data_out	( data_out	),
		.full		( full		),
		.empty		( empty		)
	);

	fifo #(.DATA_WIDTH(32), .FIFO_DEPTH(256)) dut (
		.CLK		( clk		),
		.rST		( rst		),
		.ReadEn		( re		),
		.WriteEn	( we		),
		.DataIn		( data_in	),
		.DataOut	( data_out	),
		.Full		( full		),
		.Empty		( empty		)
	);

	// Clock generation
	initial begin: clk_gen
		clk <= 0;
		#clockHigh;
		forever 
		begin
			#clockLow  clk <= 1;
			#clockHigh clk <= 0;
		end
	end

	// Reset generation
	initial begin: rst_gen
		rst = 1;
		#NUM_OF_EDGES_RESET;
		rst = 0;
	end
endmodule
