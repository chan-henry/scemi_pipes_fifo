// FifoTransactor
//
//	This is going into the emulator
//	and will be driven by its proxy
//
//	FIXED: Get rid of the hard # delays
//	FIXED: reset the BFM from the top reset
//	TODO: convert pipe tasks to functions so they don't consume clock time
//	TODO: allow initialization from proxy
//	TODO: allow reset from proxy

module FifoTransactor
( 
	input				clk,
	input				rst,

	input		[ 31 : 0 ]	data_out,
	input				full,
	input				empty,

	output reg			re,
	output reg			we,
	output reg	[ 31 : 0 ]	data_in
);

	// Write Variables
	int dataNumToWrite	= 0;	// Write count
	bit writeStart		= 0;	// Write start flag
	int num_elements_valid	= 0;	// Placeholder, never really used
	bit writeProcessing	= 0;	// Read state flag
	bit eom;			// End of message

	// Read Variables
	int dataNumToRead	= 0;	// Read count
	bit readStart		= 0;	// Read start flag
	bit readProcessing	= 0;	// Read state flag

	reg [256*32-1:0] write_data_vector;
	reg [256*32-1:0] read_data_vector;
	reg [31:0] data_array[256];

	// Pipe instantiations
	scemi_input_pipe #(
		.BYTES_PER_ELEMENT( 4 ), 	// 4 byte elements
		.PAYLOAD_MAX_ELEMENTS( 256 ),	// 1 element per payload
		.BUFFER_MAX_ELEMENTS( 256 ),	// 64 elements in the buffer
		.VISIBILITY_MODE( 1 ),
		.IS_CLOCKED_INTF( 1 ) )
	input_pipe( clk );

	scemi_output_pipe #(
		.BYTES_PER_ELEMENT( 4 ), 	// 4 byte elements
		.PAYLOAD_MAX_ELEMENTS( 256 ),	// 1 element per payload
		.BUFFER_MAX_ELEMENTS( 256 ),	// 64 elements in the buffer
		.VISIBILITY_MODE( 1 ),
		.IS_CLOCKED_INTF( 1 ) )
	output_pipe( clk ); 

	// Import Functions from the proxy
	import "DPI-C" context function void writeDataAck();
	import "DPI-C" context function void readDataAck();

	// Export Functions to the proxy
	export "DPI-C" function FifoTransactorInit;	// TODO: this function should be called from the proxy
	function void FifoTransactorInit( );
		we		= 0;
		re		= 0;
		data_in		= 0;
	endfunction

	export "DPI-C" function writeData;
	function void writeData( input int _dataNum );
		dataNumToWrite	<= _dataNum;
		writeStart = 1;
	endfunction

	export "DPI-C" function readData;
	function void readData( input int _dataNum );
		dataNumToRead = _dataNum;
		readStart = 1;
	endfunction

	// Always blocks
	always @( posedge clk ) begin
	end

	always @( posedge clk ) begin: main_block
		if( 1 == rst ) begin
			FifoTransactorInit();
		end
		else begin
			if( dataNumToWrite > 0 ) begin			// If there is data to write
				if( 1 == writeStart ) begin
					writeProcessing = 1;			// Set write state flag
					we = 1'b1;				// It can receive, WRITE!
					writeStart = 0;
	
					// Blocking task
					input_pipe.receive(	.num_elements_valid(num_elements_valid),
								.num_elements(256),
								.data(write_data_vector),
								.eom(eom)
								);
	
					for( integer k=0; k < 256; k++ ) begin
						data_array[k] = write_data_vector[32*k +: 32];
					end
	
				end
				data_in = data_array[dataNumToWrite-1];
				dataNumToWrite--;			// decrement write count
			end
			else begin
				if( writeProcessing == 1 ) begin	// Just ended write processing
					writeProcessing = 0;		// Set write state flag appropriately to 0
					we = 1'b0;			// Deassert the write enable
					writeDataAck();			// Write Data Done
				end
			end
	
			if( dataNumToRead > 0 ) begin			// If received command from proxy to read
				if( 1 == readStart ) begin
					readProcessing = 1;		// Set read state flag
					re = 1'b1;			// Assert read enable
					@( posedge clk );
					@( posedge clk );
					readStart = 0;
				end
	
		// Only read data_out, after the clock that asserts re and another clock cycle after the BFM has accepted re
		//			 __    __    __
		//	clk		|  |__|  |__|  |__
		//			  ________________
		//	re		_/
		//			_______ ________ _
		//	data_out	_______X__data__X_
		//				    ^---sample here
	
				data_array[dataNumToRead-1] = data_out;
				dataNumToRead--;
			end
			else begin
				if( readProcessing == 1 ) begin
					readProcessing = 0;
					re = 1'b0;
	
					for( integer j=0; j < 256; ++j ) begin
						read_data_vector[32*j +: 32] = data_array[j];
					end
	
					output_pipe.try_send(	.byte_offset(0),
								.num_elements(256),
								.data(read_data_vector),
								.eom(0)
								);
	
					output_pipe.try_flush();
	
					readDataAck();
				end
			end
		end
	end: main_block

endmodule: FifoTransactor
