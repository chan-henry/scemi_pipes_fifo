module TB;
	// TODO: Parameterize/Macro the depth
	// TODO: get rid of the hard # delays

	import SvFifoTransactorPkg::*;

	SvFifoTransactor fifoXtor;
	
	integer depth = 256;

	bit [ 31 : 0 ]	wdata[255:0];	// 32-bit wide array with 256 elements
	bit [ 31 : 0 ]	rdata[255:0];	// 32-bit wide array with 256 elements

	bit		full;
	bit		empty;


	initial begin

		#0;	// Make sure this block starts before non-zero initial blocks

		fifoXtor = new( "top.transactor" );	// Assign handle to HDL transactor
				
		for ( integer i = 0; i < depth; i++ ) begin: random_loop	// Fill the wdata array
			wdata[i] = $random;
			$display( "random_loop wdata[%3d]: %0h", i, wdata[i] );
		end: random_loop

		#5ps;	// Wait for DUT to get out of reset

		$display( "Begin writeData function" );
		fifoXtor.writeData( wdata );	// Fill the input pipe with wdata[]

		$display( "Begin readData function" );
		fifoXtor.readData( rdata );	// Empty the output pipe into rdata[]

		for(integer i = 0; i < depth; i++ ) begin: print_results
			$display( "wdata[%3d]: %0h, rdata[%3d]: %0h", i, wdata[i], i, rdata[i] );
		end: print_results

		if ( wdata == rdata )
			$display( "wdata == rdata" );
		else
			$display( "wdata != rdata" );

		// TODO: Check full and empty signals

		#10;

		$finish;
	end
endmodule: TB
