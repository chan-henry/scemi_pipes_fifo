`ifndef _AxSvFifoTransactor_svh_
`define _AxSvFifoTransactor_svh_

/*

FifoTransactor proxy

	This proxy is instantiated in the testbench and used to control the FIFO BFM.

	TODO: parameterize this proxy
	TODO: manage the full and empty signals
	TODO: examine the callback
	TODO: examine the unused pipe variables

*/

import scemi_pipes_pkg::*;
import svdpi::*;

typedef class SvFifoTransactor;


class callback_class extends scemi_pipe_notify_callback;
	local SvFifoTransactor xtor_;
	function new( SvFifoTransactor xtor );
		xtor_ = xtor;
	endfunction

	function void notify();
		xtor_.notify_output_callback();
	endfunction
endclass


class SvFifoTransactor;

	bit eom;
	int num_elements_valid;

	/* TODO: We'll use these later
	bit full;
	bit empty;
	*/
	
	local event E_writeDataDone_;
	local event E_readDataDone_;
	
	static SvFifoTransactor fifoManager[chandle];
	local chandle hdl_scope_;
	
	local string hdlPathInput_;
	local string hdlPathOuput_;
	
	scemi_dynamic_input_pipe inputPipe_;
	scemi_dynamic_output_pipe outputPipe_;

	local int unsigned input_byte_per_elem_;	// TODO: Never used?
	local int unsigned input_get_depth_;		// TODO: Never used?
	local int unsigned output_byte_per_elem_;	// TODO: Never used?
	local int unsigned output_get_depth_;		// TODO: Never used?
	
	callback_class callback;
	
	local event wait_for_output_callback;
	static event wait_for_output_callback_non_blocking;
	
	/**
	 * Constructor of proxy
	 * @param hdlPath path to transactor on hw side
	 */
	function new( string hdlPath );
		hdl_scope_ = svGetScopeFromName( hdlPath );	// Get HDL scope
		svc_setScope_SvFifoTransactorPkg();		// 5.6.3.3 Set package scope
		fifoManager[ hdl_scope_ ] = this;		// TODO: find out what this is
		
		hdlPathInput_ = {hdlPath, ".input_pipe" };	// hierarchical reference to the input pipe
		hdlPathOuput_ = {hdlPath, ".output_pipe"};	// hierarchical reference to the output pipe
		
		inputPipe_	= new( hdlPathInput_ );		// instantiate input pipe on HVL side
		outputPipe_	= new( hdlPathOuput_ );		// instantiate output pipe on HVL side
		
		// Get characteristics of HDL input pipe
		input_byte_per_elem_	= inputPipe_.get_bytes_per_element();
		input_get_depth_	= inputPipe_.get_depth();

		// Get characteristics of HDL output pipe
		output_byte_per_elem_	= outputPipe_.get_bytes_per_element();
		output_get_depth_	= outputPipe_.get_depth();
		
		callback = new( this );			// TODO: find out what this is
	endfunction
	
	function void notify_output_callback();
		-> wait_for_output_callback;
		->> wait_for_output_callback_non_blocking;
	endfunction

	task automatic writeData( const ref bit [ 31 : 0 ] _data[255:0] );
		int unsigned data_sent;
		bit[7:0] dataTmp[];
		dataTmp = new [256*4];

		for( integer i=0; i < (256*4); i=i+4 ) begin
			{dataTmp[i+3], dataTmp[i+2], dataTmp[i+1], dataTmp[i]} = _data[i/4];
			$display(	"dataTmp[%0d-%0d]: %0h%0h%0h%0h",
					i+3,
					i,
					dataTmp[i+3],
					dataTmp[i+2],
					dataTmp[i+1],
					dataTmp[i]
				);
		end

		data_sent =  inputPipe_.try_send_bytes(	.byte_offset(0),	// Send data through the input pipe
							.num_elements(256),
							.data(dataTmp),
							.eom(1'b0)
							);
		svc_writeData( hdl_scope_, 256 );	// Start BFM write func and send write count to BFM

		if( data_sent != 256 )
			$display("ERROR - data does not fit into input pipe");

		$display("Number of elements successfully sent to input pipe: %0d", data_sent);

		inputPipe_.flush();	// Blocks until the consumer (the HDL BFM) receives all data
		
		//_full = full;		//TODO
		//_empty = empty;	//TODO

		@E_writeDataDone_;	// We're done with the input pipe
	endtask
	
	task automatic readData( ref bit [ 31 : 0 ] _data[255:0] );
		int data_receive;
		bit[7:0] dataTmp[];
		dataTmp = new [256*4];
		
		outputPipe_.set_notify_callback( callback, 0 );
		
		svc_readData( hdl_scope_,  256 );
		//@wait_for_output_callback;
		//@wait_for_output_callback_non_blocking;
		
		outputPipe_.receive_bytes(	.num_elements_valid(num_elements_valid),
						.num_elements(256),
						.data(dataTmp),
						.eom(eom)
						);

		for( integer i=0; i < (256*4); i=i+4 ) begin
			_data[i/4] = {dataTmp[i+3], dataTmp[i+2], dataTmp[i+1], dataTmp[i]};
			//$display( "read_data[%3d]: %0h", i/4, _data[i/4] );
		end

		@E_readDataDone_;
	endtask
	
	function void SvReadDataAck();
		->E_readDataDone_;
	endfunction
	
	function void SvWriteDataAck();
		->E_writeDataDone_;
	endfunction
endclass

/* DPI functions for connection with HW side */
import "DPI-C" context function void svc_setScope_SvFifoTransactorPkg();

import "DPI-C" context function void svc_writeData( input chandle _scope, input int _dataNum );

import "DPI-C" context function void svc_readData( input chandle _scope, input int _dataNum );

export "DPI-C" function svc_writeDataAck;
function void svc_writeDataAck( input chandle _scope );
	automatic SvFifoTransactor ptr = SvFifoTransactor::fifoManager[ _scope ];
	ptr.SvWriteDataAck();
endfunction 

export "DPI-C" function svc_readDataAck;
function void svc_readDataAck( input chandle _scope );
	automatic SvFifoTransactor ptr = SvFifoTransactor::fifoManager[ _scope ];
	ptr.SvReadDataAck();
endfunction 

`endif // _AxSvMemoryTransactor_svh_
